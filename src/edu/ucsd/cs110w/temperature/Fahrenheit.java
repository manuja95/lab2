/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author mgunarat
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return ""+this.getValue()+" F";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float value = (super.getValue() -32)*5/9;
		return new Celsius(value);
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit(super.getValue());
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return new Fahrenheit((super.getValue() + 460) * (5/9) );
	}
	
	
}

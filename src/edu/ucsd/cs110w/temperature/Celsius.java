/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author mgunarat
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return ""+super.getValue()+" C ";
	}
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius(super.getValue());
	}
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float f = (((super.getValue())*9)/5)+32;
		return new Celsius(f);
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return new Celsius(super.getValue() + 273);
	}
}
/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (mgunarat): write class javadoc
 *
 * @author mgunarat 
 */
public class Kelvin extends Temperature{

	public Kelvin(float v) {
		super(v);
		// TODO Auto-generated constructor stub
	}
	
	public String toString() 
	{ 
	 // TODO: Complete this method 
		return ""+super.getValue()+"  K"; 
	} 
	@Override 
	public Temperature toCelsius() { 
	 
	 return new Kelvin(super.getValue() - 273); 
	} 
	@Override 
	public Temperature toFahrenheit() { 
	 // TODO: Complete this method 
	 return new Kelvin(super.getValue() * (9/5) - 460); 
	}

	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return new Kelvin(super.getValue());
	}

}
